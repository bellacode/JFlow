package bp.gpm.menu2020;

import bp.en.EntityTreeAttr;

/** 
菜单
*/
public class MenuAttr extends EntityTreeAttr
{
	/** 
	 控制方法
	*/
	public static final String MenuCtrlWay = "MenuCtrlWay";
	/** 
	 系统
	*/
	public static final String ModuleNo = "ModuleNo";
	/** 
	 系统编号
	*/
	public static final String SystemNo = "SystemNo";
	/** 
	 图片
	*/
	public static final String WorkType = "WorkType";
	/** 
	 连接
	*/
	public static final String Url = "Url";
	/** 
	 连接（pc）
	*/
	public static final String UrlExt = "UrlExt";
	/** 
	 连接（移动端）
	*/
	public static final String MobileUrlExt = "MobileUrlExt";
	/** 
	 控制内容
	*/
	public static final String CtrlObjs = "CtrlObjs";
	/** 
	 是否启用
	*/
	public static final String IsEnable = "IsEnable";
	/** 
	 打开方式
	*/
	public static final String OpenWay = "OpenWay";
	/** 
	 标记
	*/
	public static final String Mark = "Mark";
	/** 
	 扩展1
	*/
	public static final String Tag1 = "Tag1";
	/** 
	 扩展2
	*/
	public static final String MenuModel = "MenuModel";
	/** 
	 列表模式
	*/
	public static final String ListModel = "ListModel";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 图标
	*/
	public static final String Icon = "Icon";

	public static final String FrmID = "FrmID";
	public static final String FlowNo = "FlowNo";

}
